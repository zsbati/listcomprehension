if __name__ == '__main__':

    x = int(input())
    y = int(input())
    z = int(input())
    n = int(input())

    list_x = [a for a in range(x+1)]
    list_y = [b for b in range(y+1)]
    list_z = [c for c in range(z+1)]

    permutations = [[a, b, c] for a in list_x for b in list_y for c in list_z if a+b+c !=n]


    print(permutations)
